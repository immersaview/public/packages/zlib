#!/bin/bash

# Exit as failure if any command fails
set -e

pkg_id=$(grep -oPm1 "(?<=<id>)[^<]+" *.nuspec)
pkg_version=$(grep -oPm1 "(?<=<version>)[^<]+" *.nuspec)

if curl --silent "${CI_SERVER_URL}/api/v4/projects/${CI_PROJECT_ID}/packages/nuget/query?q=${pkg_id}" | grep \"${pkg_version}\" -q; 
then
    echo "Package with existing version was found."
    exit 1
else
    exit 0
fi