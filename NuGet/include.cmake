cmake_minimum_required(VERSION 3.0)

# Declaring variables
set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}")
set(LIB_NAME "")
set(LIB_NAME_DEBUG_SUFFIX "")

if ("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "aarch64")
    set(ARCH_DIR "arm64")
elseif ("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "AMD64|x86_64")
    if("${CMAKE_SIZEOF_VOID_P}" STREQUAL "4")
        set(ARCH_DIR "x86")
    elseif ("${CMAKE_SIZEOF_VOID_P}" STREQUAL "8")
        set(ARCH_DIR "x64")
    endif ()
else ()
    message(FATAL_ERROR "Unable to determine CPU architecture")
endif ()

if (UNIX)
    find_program(LSB_RELEASE_EXEC lsb_release)
    execute_process(COMMAND ${LSB_RELEASE_EXEC} --codename
        OUTPUT_VARIABLE LSB_RELEASE_CODENAME
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    if (LSB_RELEASE_CODENAME MATCHES "xenial")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/ubuntu16_04/gcc5_4_0/${ARCH_DIR}")
    elseif (LSB_RELEASE_CODENAME MATCHES "bionic")    
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/ubuntu18_04/gcc7_4_0/${ARCH_DIR}")
    elseif (LSB_RELEASE_CODENAME MATCHES "focal")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/ubuntu20_04/gcc9_3_0/${ARCH_DIR}")
    elseif (LSB_RELEASE_CODENAME MATCHES "Core")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/centos7/gcc7_2_1/${ARCH_DIR}")
    endif ()

    set(LIB_NAME "libz")
elseif (WIN32)
    set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/windows/vc142/MT/${ARCH_DIR}")
    set(PLATFORM_DIR_MD "${CMAKE_CURRENT_LIST_DIR}/windows/vc142/MD/${ARCH_DIR}")
    set(LIB_NAME "zlibstatic")
    set(LIB_NAME_DEBUG_SUFFIX "d")
endif ()

function(add_imported_lib PLATFORM_DIR LIB)
    message(STATUS "Adding library '${LIB}'. Reference the library using the TARGET '${LIB}'")
    add_library("${LIB}" STATIC IMPORTED GLOBAL)
    set_property(TARGET "${LIB}" APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_LIST_DIR}/include")
    set_property(TARGET "${LIB}" APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${PLATFORM_DIR}/include")
    set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_DEBUG "${PLATFORM_DIR}/debug/${LIB_NAME}${LIB_NAME_DEBUG_SUFFIX}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_RELEASE "${PLATFORM_DIR}/release/${LIB_NAME}${CMAKE_STATIC_LIBRARY_SUFFIX}")
endfunction(add_imported_lib)

set(ZLIB_SOURCE_DIR "${CMAKE_CURRENT_LIST_DIR}/../../src" CACHE PATH "Directory in which the ZLib source code can be found." FORCE)
mark_as_advanced(ZLIB_SOURCE_DIR)

# Add Libraries
if (MSVC)
    add_imported_lib("${PLATFORM_DIR}" "zlib_mt")
    add_imported_lib("${PLATFORM_DIR_MD}" "zlib_md")

    add_library(zlib INTERFACE)
    target_link_libraries(zlib INTERFACE zlib_mt)
else ()
    add_imported_lib("${PLATFORM_DIR}" "zlib")
endif ()
