<?xml version="1.0"?>
<package >
  <metadata>
    <!-- Package Metadata -->
    <id>Imv.External.zlib</id>
    <version>1.2.11.20220325</version>
    <title>zlib</title>
    <owners>Gerard Ryan</owners>
    <tags>native zlib compression</tags>

    <!-- Source Metadata -->
    <authors>Jean-loup Gailly (compression) and Mark Adler (decompression)</authors>
    <licenseUrl>https://zlib.net/zlib_license.html</licenseUrl>
    <projectUrl>http://zlib.net/</projectUrl>
    <iconUrl>http://imvcode/uploads/-/system/project/avatar/77/zlib_icon.png</iconUrl>
    <requireLicenseAcceptance>false</requireLicenseAcceptance>
    <summary>
      zlib is designed to be a free, general-purpose, legally unencumbered -- that is, not covered by any patents -- lossless data-compression library for use on virtually any computer hardware and operating system. 
    </summary>
    <description>
      A Massively Spiffy Yet Delicately Unobtrusive Compression Library
      (Also Free, Not to Mention Unencumbered by Patents)

      (Not Related to the Linux zlibc Compressing File-I/O Library)

      Example CMake Usage:
      target_link_libraries(target zlib)
      target_include_directories(target PRIVATE &quot;$&lt;TARGET_PROPERTY:zlib,INTERFACE_INCLUDE_DIRECTORIES&gt;&quot;)
    </description>
    <releaseNotes>
      Version 1.2.11 has these key improvements over 1.2.10:
      - Fix deflate stored bug when pulling last block from window
      - Permit immediate deflateParams changes before any deflate input
    </releaseNotes>
    <copyright>zlib software copyright © 1995-2017 Jean-loup Gailly and Mark Adler</copyright>
    <dependencies></dependencies>
  </metadata>
  <files>
    <file src="..\LICENSE" />
    <file src="..\source\contrib\dotzlib\LICENSE_1_0.txt" />
    <file src="..\source\**.h" target="build\native\include\" />
    <file src="include.cmake"  target="build\native\include.cmake" />

    <!-- Source -->
    <file src="..\source\**.h"   target="src\" />
    <file src="..\source\**.c"   target="src\" />
    <file src="..\source\**.cpp" target="src\" />
    <file src="..\source\**.hpp" target="src\" />

    <!-- Windows  Release -->
    <file src="..\Visual Studio 16 2019\x64\MT\source\Release\*.h"            target="build\native\windows\vc142\MT\x64\include" />
    <file src="..\Visual Studio 16 2019\x86\MT\source\Release\*.h"            target="build\native\windows\vc142\MT\x86\include" />
    <file src="..\Visual Studio 16 2019\x64\MD\source\Release\*.h"            target="build\native\windows\vc142\MD\x64\include" />
    <file src="..\Visual Studio 16 2019\x86\MD\source\Release\*.h"            target="build\native\windows\vc142\MD\x86\include" />
    <file src="..\Visual Studio 16 2019\x64\MT\source\Release\zlibstatic.lib" target="build\native\windows\vc142\MT\x64\release" />
    <file src="..\Visual Studio 16 2019\x86\MT\source\Release\zlibstatic.lib" target="build\native\windows\vc142\MT\x86\release" />
    <file src="..\Visual Studio 16 2019\x64\MD\source\Release\zlibstatic.lib" target="build\native\windows\vc142\MD\x64\release" />
    <file src="..\Visual Studio 16 2019\x86\MD\source\Release\zlibstatic.lib" target="build\native\windows\vc142\MD\x86\release" />

    <!-- Windows  Debug -->
    <file src="..\Visual Studio 16 2019\x64\MT\source\Debug\zlibstaticd.lib"  target="build\native\windows\vc142\MT\x64\debug" />
    <file src="..\Visual Studio 16 2019\x86\MT\source\Debug\zlibstaticd.lib"  target="build\native\windows\vc142\MT\x86\debug" />
    <file src="..\Visual Studio 16 2019\x64\MD\source\Debug\zlibstaticd.lib"  target="build\native\windows\vc142\MD\x64\debug" />
    <file src="..\Visual Studio 16 2019\x86\MD\source\Debug\zlibstaticd.lib"  target="build\native\windows\vc142\MD\x86\debug" />

    <!-- Ubuntu16 Release -->
    <!-- 
    <file src="..\Ubuntu16_04\Unix Makefiles\arm64\Release\source\*.h"     target="build\native\ubuntu16_04\gcc5_4_0\arm64\include" />
    <file src="..\Ubuntu16_04\Unix Makefiles\arm64\Release\source\*.a"     target="build\native\ubuntu16_04\gcc5_4_0\arm64\release" />
    -->

    <!-- Ubuntu16 Debug -->
    <!--
    <file src="..\Ubuntu16_04\Unix Makefiles\arm64\Debug\source\*.a"       target="build\native\ubuntu16_04\gcc5_4_0\arm64\debug" />
    -->

    <!-- Ubuntu18 Release -->
    <file src="..\Ubuntu18_04\Unix Makefiles\x64\Release\source\*.h"       target="build\native\ubuntu18_04\gcc7_4_0\x64\include" />
    <file src="..\Ubuntu18_04\Unix Makefiles\x64\Release\source\*.a"       target="build\native\ubuntu18_04\gcc7_4_0\x64\release" />

    <!-- Ubuntu18 Debug -->
    <file src="..\Ubuntu18_04\Unix Makefiles\x64\Debug\source\*.a"         target="build\native\ubuntu18_04\gcc7_4_0\x64\debug" />

    <!-- Ubuntu20 Release -->
    <file src="..\Ubuntu20_04\Unix Makefiles\x64\Release\source\*.h"       target="build\native\ubuntu20_04\gcc9_3_0\x64\include" />
    <file src="..\Ubuntu20_04\Unix Makefiles\x64\Release\source\*.a"       target="build\native\ubuntu20_04\gcc9_3_0\x64\release" />

    <!-- Ubuntu20 Debug -->
    <file src="..\Ubuntu20_04\Unix Makefiles\x64\Debug\source\*.a"         target="build\native\ubuntu20_04\gcc9_3_0\x64\debug" />

    <!-- CentOS  Release -->                                               
    <file src="..\CentOS7\Unix Makefiles\x64\Release\source\*.h"           target="build\native\centos7\gcc7_2_1\x64\include" />
    <file src="..\CentOS7\Unix Makefiles\x64\Release\source\*.a"           target="build\native\centos7\gcc7_2_1\x64\release" />

    <!-- CentOS Debug -->
    <file src="..\CentOS7\Unix Makefiles\x64\Debug\source\*.a"             target="build\native\centos7\gcc7_2_1\x64\debug" />
  </files>
</package>
