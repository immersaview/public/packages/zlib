**Nuget packaging for zlib Repository**

## Example CMake Usage:
```cmake
target_link_libraries(target zlib)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:zlib,INTERFACE_INCLUDE_DIRECTORIES>")
```