#!/usr/bin/env bash

# Usage:
# build <generator> <arch> <config> <OS> <runtime>

# Exit as failure if any command fails
set -e

if [[ -z "$1" || "$1" == "-h" || "$1" == "--help" ]]; then
    echo "$0 <generator> <arch> <config> <OS> <runtime>"
    exit 0
fi

BASE_DIR="`cd \`dirname ${BASH_SOURCE}\` && pwd`"
GENERATOR="$1"
ARCH="$2"
CONFIG="$3"
OS="$4"
RUNTIME="$5"

if [[ "${GENERATOR}" == "Visual Studio"* ]]; then
    if [[ "${ARCH}" == "x86" ]]; then
        CMAKE_ARGS="-A Win32"
    else
        CMAKE_ARGS="-A ${ARCH}"
    fi

    CMAKE_RUNTIME="-DIMV_MSVC_RUNTIME=${RUNTIME}"

    BUILD_DIR="${BASE_DIR}/${GENERATOR}/${ARCH}/${RUNTIME}"
    BUILD_ARGS="/consoleloggerparameters:ForceConsoleColor /maxcpucount:`nproc`"
elif [[ "${GENERATOR}" == "Unix Makefiles" ]]; then
    BUILD_DIR="${BASE_DIR}/${OS}/${GENERATOR}/${ARCH}/${CONFIG}"
    BUILD_ARGS="-j `nproc` VERBOSE=1"
else
    echo "Unsupported generator"
    exit 1
fi

rm -rf "${BUILD_DIR}"
mkdir -p "${BUILD_DIR}"

pushd "${BUILD_DIR}"

    cmake -G "${GENERATOR}" ${CMAKE_ARGS} ${CMAKE_RUNTIME} -DCMAKE_BUILD_TYPE=${CONFIG} ${CMAKE_FLAGS} "${BASE_DIR}"
    cmake --build . --config ${CONFIG} -- ${BUILD_ARGS}

    if [[ "${GENERATOR}" == "Visual Studio"* ]]; then
        mv ./source/zconf.h ./source/${CONFIG}
        mv ./source/zlib.pc ./source/${CONFIG}
    fi

popd
